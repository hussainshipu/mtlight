import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index.vue'
import map from '@/components/map.vue'
import information from '@/components/information.vue'
import selectedLoction from '@/components/selectedLoction.vue'
import contact from '@/components/contact.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/map',
      name: 'map',
      component: map
    },
    {
      path: '/information',
      name: 'information',
      component: information
    },
    {
      path: '/selectedLoction',
      name: 'selectedLoction',
      component: selectedLoction
    },
    {
      path: '/contact',
      name: 'contact',
      component: contact
    },
  ]
})
